package com.example.democode

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.democode.ui.theme.DemoCodeTheme

class MainActivity : ComponentActivity() {
    private var itemArray: Array<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DemoCodeTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    //Call the composable to get the string array
                    itemArray = itemsFromResources()
                    //Display the string items
                    ItemList(itemArray = itemArray as Array<out String>)
                }
            }
        }
    }
}

@Composable
fun itemsFromResources(): Array<String> {
    //Get the string array from the String Resources
    val itemArray: Array<String> = stringArrayResource(R.array.car_models)

    return itemArray
}

@Composable
fun ItemList(itemArray: Array<out String>){
    /* Array<out String> means that the array can hold elements of type
       'string' or any subtype of 'string', such as 'CharSequence'.
   */

    //Use a LazyColumn to display the items
    LazyColumn{
    items(itemArray.size) {index ->
        Text(text= itemArray[index])
        }
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    DemoCodeTheme {
        //Get and display the array of string
        ItemList(itemsFromResources())
    }
}
